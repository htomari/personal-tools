#!/bin/sh
set -e
searchdir="$@"
for tpl in jpg,/ mp4,/ arw,/sony/ dng,/PENTAX/ orf,/Olympus/; do
	IFS=","
	set -- $tpl
	find $searchdir -type f -iname "*.$1" -exec /bin/sh -c '
		dst="${HOME}/Pictures$2$(echo "$1" | tr A-Z a-z)"
		dstdir=`dirname "$dst"`
		if [ ! -d "$dstdir" ] ; then
			mkdir -p "$dstdir"/
		fi
		cp --preserve=timestamps "$1" "$dst"
	' /bin/sh {} "$2" \;
done

