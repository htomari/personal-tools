#!/bin/sh
# usage: scalewebp.sh [-rotate (90|180|270)] source.jpg dest.webp res
MIN_EXIF_SIZE=400 # minimum image resolution to retain exif metadata
set -e
rotcmd="cat"
exifcmd="cat"
if [ a$1 = "a-rotate" ] ; then
	opt1="$1"
	opt2="$2"
	shift 2
	rotcmd="pnmflip $opt1$opt2"
elif [ ! -f "$1" ] ; then
	echo "Usage: "`basename $0`" [-rotate (90|180|270)] source.jpg target.webp resolution"
	exit 1
fi
#if [[ "$3" -ge "$MIN_EXIF_SIZE" ]] ; then
#	exifcmd="exiftool -overwrite_original -tagsFromFile "$1" -x Orientation -x ThumbnailImage -x PreviewImage -"
#fi
tmppng=$(mktemp --suffix=.png)
jpegtopnm "$1" | \
$rotcmd | \
pnmscale -xysize "$3" "$3" | \
pnmtopng > "$tmppng"
cwebp -m 6 -pass 10 -af -hint photo "$tmppng" -o "$2"

unlink "$tmppng"


