#!/usr/bin/perl
use strict;
use warnings;
use File::Basename;
use HTML::Entities;
#use Graphics::Magick;
use Image::Magick;

my $prefix="img/";
my $class="ss";
my $alt="";
my $opt_done=0;

while(!$opt_done && scalar @ARGV > 0) {
	if($ARGV[0] eq '-prefix') {
		shift;
		$prefix=shift;
	} elsif($ARGV[0] eq '-class') {
		shift;
		$class=shift;
	} elsif($ARGV[0] eq '-alt') {
		shift;
		$alt=shift;
	} else {
		$opt_done=1;
	}
}

foreach my $file (@ARGV) {
	next if($file =~ /_s\.jpg$/);
	my $thumb_file=$file;
	$thumb_file =~ s/\.jpg/_s.jpg/;
	my $webp_thumbnail =$file;
	$webp_thumbnail =~ s/\.jpg/_s.webp/;

	my $img=Image::Magick->new;
	$img->Read($thumb_file);
	my $dim='height="'.$img->Get('height').'" width="'.$img->Get('width').'"';
	my $html_path=encode_entities($prefix.$file);
	my $html_thumb=encode_entities($prefix.$thumb_file);
	my $webp_thumb=encode_entities($prefix.$webp_thumbnail);

	my $webp_exists=-e $webp_thumbnail;

	print qq(<a href="$html_path">);
	if($webp_exists){
		print qq(<picture><source type="image/webp" srcset="$webp_thumb">);
	}
	print qq(<img src="$html_thumb" $dim alt="$alt" loading=lazy class="$class">);
	if($webp_exists){
		print qq(</picture>);
	}
	print qq(</a>\n);
}
