#!/bin/sh
sed -i 's/[0-9A-Fa-f]\{1,2\}:[0-9A-Fa-f]\{1,2\}:[0-9A-Fa-f]\{1,2\}:[0-9A-Fa-f]\{1,2\}:[0-9A-Fa-f]\{1,2\}:[0-9A-Fa-f]\{1,2\}/xx:xx:xx:xx:xx:xx/g' "$@"
sed -i 's/[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}/xxx.xxx.xxx.xxx/g' "$@"
sed -i 's/[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx/g' "$@"
sed -r --in-place 's/=[0-9a-fA-F]{64}/=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx/g' "$@"
perl -i -ane 'if(/^(.*Serial Number: *)(.*)/) { my $hd=$1; my $tmp=$2; $tmp =~ s/[a-zA-Z0-9]/x/g; print $hd.$tmp."\n"; } else {print ;}' "$@"
