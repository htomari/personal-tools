#!/bin/sh
## Generates scaled-down JPG and its thumbnail.
## usage:
## scalejpg2.sh [-rotate (90|180|270)] source.jpg target_basename [res [res_s]]
##  => yields target_basename.jpg at resolution res and
##            target_basename_s.jpg at resolution res_s  
set -e
SCALEJPG="scalejpg.sh"
SCALEWEBP="scalewebp.sh"
RES=1280
RES_S=160
ROT_OPTS=""
if [ a$1 = "a-rotate" ] ; then
	opt1="$1"
	opt2="$2"
	shift 2
	ROT_OPTS="$opt1 $opt2"
elif [ ! -f "$1" ] ; then
	echo "Usage: "`basename $0`" [-rotate (90|180|270)] source.jpg target_basename res res_s"
	exit 1
fi
if [ -n "$3" ] ; then
	RES="$3"
	if [ -n "$4" ]; then
		RES_S=$4
	fi
fi
"$SCALEJPG" $ROT_OPTS "$1" "${2}.jpg" "$RES" &
pid1=$!
"$SCALEJPG" $ROT_OPTS "$1" "${2}_s.jpg" "$RES_S" &
pid2=$!
"$SCALEWEBP" $ROT_OPTS "$1" "${2}_s.webp" "$RES_S" &
pid3=$!
#"$SCALEWEBP" $ROT_OPTS "$1" "${2}.webp" "$RES" &
#pid4=$!

wait $pid1 $pid2 $pid3 # $pid4

