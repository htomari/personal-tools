/* リサイズ済みの画像(JPG, AVIF, WEBP) を指定すると
   いい感じのHTMLを出力してくれる
 */
extern crate image;
extern crate html_escape;

use std::env;
use std::path::Path;
use std::collections::HashMap;
use image::image_dimensions as ImageDimensions;
use html_escape::encode_double_quoted_attribute as htmlescape;

struct ImageEntry {
	dim: Option<(u32,u32)>,
	is_thumbnail: bool, // Thumbnail なら本体画像へのリンクを作る必要あり
	has_webp: bool,
	has_avif: bool
}
impl ImageEntry {
	fn new() -> ImageEntry {
		ImageEntry {
			dim: None,
			is_thumbnail: false,
			has_webp: false,
			has_avif: false
		}
	}
}

fn break_image_file_name(filename: &String) -> (String, bool) {
	if filename.ends_with("_s") {
		let slice=&filename[0..(filename.len()-2)];
		(String::from(slice), true)
	} else {
		(filename.to_string(), false)
	}
}

fn create_img(img_name: &String, attrs: &ImageEntry) -> String {
	let (width,height)=attrs.dim.unwrap();
	format!("<img src=\"{}.jpg\" class=\"ss\" height={} width={} loading=lazy alt=\"\">",htmlescape(img_name),height,width)
}

fn create_pic(img_name: &String, attrs: &ImageEntry) -> String {
	let img=create_img(img_name,attrs);
	if attrs.has_webp || attrs.has_avif {
		let mut s=String::from("<picture>\n");
		if attrs.has_avif {
			s.push_str(format!("\t<source type=\"image/avif\" srcset=\"{}.avif\">\n",htmlescape(img_name)).as_str());
		}
		if attrs.has_webp {
			s.push_str(format!("\t<source type=\"image/webp\" srcset=\"{}.webp\">\n",htmlescape(img_name)).as_str());
		}
		s.push('\t');
		s.push_str(img.as_str());
		s.push('\n');
		s.push_str("</picture>");
		s
	} else {
		img
	}
}

fn create_a(img_name: &String, attrs: &ImageEntry) -> String {
	if attrs.is_thumbnail {
		let pic=create_pic(&(img_name.clone()+"_s"),attrs);
		String::from(format!("<a href=\"{}.jpg\">{}</a>",img_name,pic))
	} else {
		create_pic(img_name,attrs)
	}
}

fn main() {
	// どういう順序で画像が渡されるかわからないので、いったん何が渡されたか全部見る。
	let mut images=HashMap::new();
	for argument in env::args() {
		let img_path=Path::new(&argument);
		let file_stem=img_path.file_stem().unwrap().to_string_lossy().to_string();
		let (image_name, is_thumbnail) = break_image_file_name(&file_stem);
		let mut ie=images.entry(image_name).or_insert(ImageEntry::new());

		let path_extension=img_path.extension().unwrap_or_default();
		match path_extension.to_string_lossy().into_owned().as_str() {
			"jpg" => {
				if !ie.is_thumbnail { // すでにサムネール発見済みなら何もやることはない
					ie.dim=ImageDimensions(&argument).ok();
					ie.is_thumbnail=is_thumbnail;
				}
			},
			"webp" => {
				ie.has_webp = true;
			},
			"avif" => {
				ie.has_avif = true;
			},
			_unknown_extension=>{
			}
		}
	}
	for (img_name, attrs) in images {
		if !attrs.dim.is_none() { // 画像サイズが取得できてれば有効なエントリとみなせる
			let prefixed_img_name=String::from("img/")+&img_name;
			println!("{}",create_a(&prefixed_img_name, &attrs));
		}
	}
}
