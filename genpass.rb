#!/usr/bin/ruby
# password generator 2
# 2013 H.Tomari
# usage: genpass [-a] [-n] [-s] [-x string] [length]
#                -a: turn off alphabets
#                -n: turn off numbers
#                -s: turn on special characters
#                -x: append string to the candidates
#                length: length of password

def anzenRansu(saidaichi)
  if !@sslMoji || (@sslMoji.length()-@sslMojinoBasho<4)
    @sslMoji=`openssl rand -hex 512`
    @sslMojinoBasho=0
  end
  moji=@sslMoji[@sslMojinoBasho,4]
  kazu=Integer("0x"+moji)
  @sslMojinoBasho+=4
  kekka=Integer(kazu/Float(0xffff)*saidaichi)
  kekka
end

def mojiErabu(moji,nagasa)
  kekka=Array(nagasa)
  nagasa.times { |i|
    kekka[i]=moji[anzenRansu(moji.length)]
  }
  kekka.join()
end

ALPHABET='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
SUUJI='0123456789'
TOKUBETSU='!#$%&()[]{}`"\'~-^@+*.,/_\\'
def argYomu(arg)
  alphaTsukau=1
  suujiTsukau=1
  tokubetsuTsukau=0
  kouho=""
  nagasa=16
  rarg=arg.reverse()
  while k=rarg.pop()
    case k
      when "-a"
        alphaTsukau=0
      when "-n"
        suujiTsukau=0
      when "-s"
        tokubetsuTsukau=1
      when "-x"
        kouho=rarg.pop()
      else
        nagasa=Integer(k)
    end
  end
  if alphaTsukau>0
    kouho+=ALPHABET
  end
  if suujiTsukau>0
    kouho+=SUUJI
  end
  if tokubetsuTsukau>0
    kouho+=TOKUBETSU
  end
  mojiErabu(kouho,nagasa)
end

print argYomu(ARGV)+"\n"

